<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'admin']], function ()
{

Route::resource('/users', 'UsersController', ['except' => [
      'update', 'show']]);

Route::resource('/places', 'PlacesController', ['except' => [
      'update', 'show']]);

Route::resource('/place_categories', 'place_categoriesController');

Route::resource('/locations', 'LocationController');

});

Route::resource('users', 'UsersController', ['except' => [
     'create', 'destroy', 'index', 'store']]);

Route::resource('places', 'PlacesController', ['except' => [
     'create', 'destroy', 'index', 'store']]);
			

Route::post('admin/users/search', 'SearchController@search');

Route::post('admin/places/search', 'SearchController@searchplace');

Route::get('/rooms', 'RoomsController@indexrooms');

Route::get('place/{place}/rooms', [
        'as'   => 'rooms.index',
        'uses' => 'RoomsController@index'
    ]);

Route::get('place/{place}/rooms/create', [
        'as'   => 'rooms.create',
        'uses' => 'RoomsController@create'
    ]);
Route::post('place/{place}/rooms', [
        'as'   => 'rooms.store',
        'uses' => 'RoomsController@store'
    ]);
Route::get('place/{place}/rooms/{room}', [
        'as'   => 'rooms.show',
        'uses' => 'RoomsController@show'
    ]);

Route::get('place/{place}/rooms/{room}/edit', [
        'as'   => 'rooms.edit',
        'uses' => 'RoomsController@edit'
    ]);

Route::put('place/{place}/rooms/{room}', [
        'as'   => 'rooms.update',
        'uses' => 'RoomsController@update'
    ]);

Route::delete('place/{place}/rooms/{room}', [
        'as'   => 'rooms.destroy',
        'uses' => 'RoomsController@destroy'
    ]);