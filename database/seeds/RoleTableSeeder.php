<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->timestamps = false;
        $role_admin->save();

        $role_gerant = new Role();
        $role_gerant->name = 'gérant de lieu';
        $role_gerant->timestamps = false;
        $role_gerant->save();

        $role_prestataire = new Role();
        $role_prestataire->name = 'prestataire';
        $role_prestataire->timestamps = false;
        $role_prestataire->save();

        $role_utilisateur = new Role();
        $role_utilisateur->name = 'utilisateur';
        $role_utilisateur->timestamps = false;
        $role_utilisateur->save();
    }
}
