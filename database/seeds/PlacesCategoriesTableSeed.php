<?php

use Illuminate\Database\Seeder;
use App\Place_categorie;

class PlacesCategoriesTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $location_urbain = new Place_categorie();
        $location_urbain->name = 'Urbain';
        $location_urbain->timestamps = false;
        $location_urbain->save();

        $location_campagne = new Place_categorie();
        $location_campagne->name = 'Campagne';
        $location_campagne->timestamps = false;
        $location_campagne->save();

        $location_mer = new Place_categorie();
        $location_mer->name = 'Mer';
        $location_mer->timestamps = false;
        $location_mer->save();

        $location_montagne = new Place_categorie();
        $location_montagne->name = 'Montagne';
        $location_montagne->timestamps = false;
        $location_montagne->save();
    }
}
