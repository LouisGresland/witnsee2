<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('postal_code', 10)->change();
            $table->string('civility', 3)->change();
            $table->boolean('society')->change();
            $table->string('phone', 15)->change();
        });

        Schema::table('roles', function ($table) {
            $table->string('name', 15)->change();
        });

        Schema::table('places', function ($table) {
            $table->string('zip_code', 10)->change();
            $table->text('description')->change();
            $table->string('phone', 15)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
