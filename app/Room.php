<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name', 'description', 'capacity', 'surface', 'equipment', 'disabled_access', 'constraints',
    ];

    public function Places()
    {
        return $this->belongsToMany('App\Place', 'places_rooms', 'room_id', 'place_id');
    }

    public function hasAnyPlaces($places)
    {
        if (is_array($places)) {
            foreach ($places as $place) {
                if ($this->hasplace($place)) {
                    return true;
                }
            }
        } else {
            if ($this->hasPlace($place)) {
                return true;
            }
        }
        return false;
    }
    
    public function hasPlace($place)
    {
        if ($this->places()->where('name', $place)->first()) {
            return true;
        }
        return false;
    }
}
