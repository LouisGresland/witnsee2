<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|alpha',
            'civility' => 'required',
            'society' => 'required',
            'phone' => 'required|max:10|min:10',
            'postal_code' => 'required',
            'country' => 'required|alpha',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            ]);

            if($_POST['society'] == '1') {
                return Validator::make($data, [
                'society_name' => 'required',
                'poste' => 'required',
                ]);

            }
        
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'first_name' => $data['first_name'],
            'civility' => $data['civility'],
            'address' => $data['address'],
            'city' => $data['city'],
            'society' => $data['society'],
            'society_name' => $data['society_name'],
            'poste' => $data['poste'],
            'postal_code' => $data['postal_code'],
            'country' => $data['country'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),

           ]);

        $user->roles()->attach(Role::where('name', 'utilisateur')->first());
        $user->save();

        return $user;

    }
}
