<?php

namespace App\Http\Controllers;

use App\User;
use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Role;

use Input;

use Session;

use Illuminate\Support\Facades\Auth;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->hasrole('admin')){
        $users = User::paginate(15);

        return view('admin.users.index', compact('users'));
        }
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hasrole('admin')){
        $id = '';
        $society= '';
        $user= new User();
        return view('admin.users.create')->with("id", $id)->with("society", $society)->with("user", $user);
        }
        return view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|alpha',
            'civility' => 'required',
            'society' => 'required',
            'phone' => 'required|max:10|min:10',
            'postal_code' => 'required',
            'country' => 'required|alpha',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|regex:/^\+(?:[0-9] ?){6,14}[0-9]$/',
            ));

            if($_POST['society'] == '1') {
                $this->validate($request, array(
                'society_name' => 'required',
                'poste' => 'required',
                ));

            }


            $user = new User();
            $user->name = $request['name'];
            $user->first_name = $request['first_name'];
            $user->civility = $request['civility'];
            $user->address = $request['address'];
            $user->city = $request['city'];
            $user->society = $request['society'];
            $user->society_name = $request['society_name'];
            $user->poste = $request['poste'];
            $user->postal_code = $request['postal_code'];
            $user->country = $request['country'];
            $user->phone = $request['phone'];
            $user->email = $request['email'];
            $user->password = bcrypt($request['password']);

            $user->save();

        
            $user->roles()->attach(Role::where('name', 'utilisateur')->first()); 


            Session::flash('success', 'L\'utilisateur a bien été ajouté.');

            $users = User::paginate(15);

        return view('admin.users.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->hasrole('admin') || Auth::id() == $id){

        $user = User::find($id);

        
        return view('admin.users.show')->withUser($user);
        }
        return view('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->hasrole('admin') || Auth::id() == $id){
        $user = User::find($id);

        $id = $user->id;
        $society = $user->society;
       

                
        return view('admin.users.edit')->withUser($user)->with("id", $id)->with("society", $society);
        }
        return view('home');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = user::find($id);
       
        $this->validate($request, array(
            'name' => 'required|max:255',
            'first_name' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|alpha',
            'civility' => 'required',
            'society' => 'required',
            'phone' => 'required|max:10|min:10',
            'postal_code' => 'required',
            'country' => 'required|alpha',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            

            ));

                if($request->input('showpassword') == '1')
            {
                $this->validate($request, array(
                    'password' => 'required|min:6|confirmed'
                    ));
            }

            

            $user->name = $request['name'];
            $user->first_name = $request['first_name'];
            $user->civility = $request['civility'];
            $user->address = $request['address'];
            $user->city = $request['city'];
            $user->society = $request['society'];
            $user->society_name = $request['society_name'];
            $user->poste = $request['poste'];
            $user->postal_code = $request['postal_code'];
            $user->country = $request['country'];
            $user->phone = $request['phone'];
            $user->email = $request['email'];
            $user->password = bcrypt($request['password']);
                
            if($user->society == '0'){
                $user->society_name = '';
                $user->poste = '';
            }

            $user->save();

            $user->roles()->detach();
        if ($request['role_user']) {
            $user->roles()->attach(Role::where('name', 'utilisateur')->first());
        }
        if ($request['role_gerant']) {
            $user->roles()->attach(Role::where('name', 'gérant de lieu')->first());
        }
        if ($request['role_prestataire']) {
            $user->roles()->attach(Role::where('name', 'prestataire')->first());
        }
        if ($request['role_admin']) {
            $user->roles()->attach(Role::where('name', 'admin')->first());
        } 

            Session::flash('success', 'Vos modifications ont bien été prise en compte.');

            $user = User::find($id);
        
        return view('admin.users.show')->withUser($user);
    

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        Session::flash('success', 'L\'utilisateur a bien été supprimé.');
        $users = User::paginate(15);
        return view('admin.users.index', compact('users'));
    }

    public function search() {
        $name = Request::input('name');
        return View('users.search')->with('tables', Table::where('name', 'like', '%' . $name . '%')->paginate(7));
    }

}
