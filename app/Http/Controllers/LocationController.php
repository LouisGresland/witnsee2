<?php

namespace App\Http\Controllers;

use Input;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Location;

use Session;

use Intervention\Image\ImageManagerStatic as Image;


class LocationController extends Controller
{

    private $rules = [
            'name' => 'required|max:14',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
               
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::paginate(15);

        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.locations.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $location = new Location;

        $this->validate($request, $this->rules);

        $this->saveLocation($request, $location);

        Session::flash('success', 'La localisation a bien été ajouté.');

        $locations = location::paginate(15);

        return view('admin.locations.index', compact('locations'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $location = Location::find($id);

        return view('admin.locations.show')->withLocation($location);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::find($id);
      
        return view('admin.locations.edit')->withLocation($location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::find($id);
       
        $this->validate($request, $this->rules);

        $this->saveLocation($request, $location);

        Session::flash('success', 'La localisation a bien été modifié.');

        $locations = Location::paginate(15);

        return view('admin.locations.index', compact('locations')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
        $location->delete();
        Session::flash('success', 'La localisation a bien été supprimé.');
        $locations = Location::paginate(15);
        return view('admin.locations.index', compact('locations'));
    }


        public function saveLocation(Request $request, Location $location)
    {
            $location->name = $request['name'];

        $image = $request->file('image');
        if ($image != null) {
        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        
        $request->image->move(public_path('images/locations'), $imageName);
        $location->image = $imageName;
            }

            $location->save();
            return $location;
    }
 
}
