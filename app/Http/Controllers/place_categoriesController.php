<?php

namespace App\Http\Controllers;

use Input;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Place_categorie;

use Session;

use Intervention\Image\ImageManagerStatic as Image;


class place_categoriesController extends Controller
{

    private $rules = [
            'name' => 'required|max:50',
            'description' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
               
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $place_categories = Place_categorie::paginate(15);

        return view('admin.place_categories.index', compact('place_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.place_categories.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $place_categorie = new Place_categorie;

        $this->validate($request, $this->rules);

        $this->savePlaceCategorie($request, $place_categorie);

        Session::flash('success', 'Le catégorie a bien été ajouté.');

        $place_categories = Place_categorie::paginate(15);

        return view('admin.place_categories.index', compact('place_categories'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place_category = Place_categorie::find($id);

        return view('admin.place_categories.show')->withPlace_category($place_category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place_category = Place_categorie::find($id);
      
        return view('admin.place_categories.edit')->withPlace_category($place_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $place_categorie = Place_categorie::find($id);
       
        $this->validate($request, $this->rules);

        $this->savePlaceCategorie($request, $place_categorie);

        Session::flash('success', 'Le catégorie a bien été modifié.');

        $place_categories = Place_categorie::paginate(15);

        return view('admin.place_categories.index', compact('place_categories')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $place_categorie = Place_categorie::find($id);
        $place_categorie->delete();
        Session::flash('success', 'La catégorie a bien été supprimé.');
        $place_categories = Place_categorie::paginate(15);
        return view('admin.place_categories.index', compact('place_categories'));
    }


        public function savePlaceCategorie(Request $request, Place_categorie $place_categorie)
    {
            $place_categorie->name = $request['name'];
            $place_categorie->description = $request['description'];
        
        $image = $request->file('image');
        if ($image != null) {
        $imageName = time().'.'.$request->image->getClientOriginalExtension();

        
        $request->image->move(public_path('images/categories'), $imageName);

        $place_categorie->image = $imageName;
            }else

            $place_categorie->save();

            return $place_categorie;
    }
 
}