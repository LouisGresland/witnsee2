<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use DB;

class SearchController extends Controller
{
    public function search(Request $request)
    {
    	
    	$users=DB::table('users')->where('name','LIKE','%'.$request->search.'%')->paginate(15);


    	return view('admin.users.search', compact('users'));

    
    }

    public function searchplace(Request $request)
    {
    	
    	$places=DB::table('places')->where('name','LIKE','%'.$request->search.'%')->paginate(15);


    	return view('admin.places.search', compact('places'));

    
    }
}

