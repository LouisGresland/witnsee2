<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Place;

use App\Http\Requests;

use App\Place_categorie;

use Session;

use App\Location;

class PlacesController extends Controller
{
    use \App\Traits\Upload_img;

    private $rules = [
            'name' => 'required|max:255',
            'location' => 'required',
            'place_category1' => 'required_without_all:place_category3,place_category5',
            'description' => 'required',
            'address' => 'required|max:255',
            'city' => 'required|alpha',
            'zip_code' => 'required|max:10',
            'country' => 'required|alpha',
            'accroche' => 'required|max:255',
            'extrait' => 'required|max:255',
            'capacity' => 'required|Integer',
            'surface' => 'required|Integer',
            'phone' => 'regex:/^\+(?:[0-9] ?){6,14}[0-9]$/',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::orderBy('id', 'desc')->paginate(15);


        return view('admin.places.index', compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $locations = Location::all();
        $places_categories = Place_categorie::all();
        $place = Place::all();
        
        return view('admin.places.create', compact('locations', 'places_categories', 'place'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $place = new Place;

        $this->validate($request, $this->rules);

        $place->description = $this->upload_img($request->input('description'));

        $this->savePlace($request, $place);

        Session::flash('success', 'Le lieu a bien été ajouté.');

        $places = Place::paginate(15);

        return view('admin.places.index', compact('places'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place = Place::find($id);
        $locations = Location::all();
        $place_categories = Place_categorie::all();

        return view('admin.places.show', compact('locations', 'place_categories'))->withPlace($place);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $place = Place::find($id);

        $locations = Location::all();
        $places_categories = Place_categorie::all();
      
        return view('admin.places.edit', compact('places_categories'), compact('locations'))->withPlace($place);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $place = Place::find($id);

       
            $this->validate($request, $this->rules);

            $place->description = $this->upload_img($request->input('description'));

            $this->savePlace($request, $place);


            Session::flash('success', 'Vos modifications ont bien été prise en compte.');

            
            $locations = Location::all();
            $place_categories = Place_categorie::all();

            return view('admin.places.show', compact('locations', 'place_categories'))->withPlace($place);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $place = Place::find($id);
        $place->delete();
        Session::flash('success', 'Le lieu a bien été supprimé.');
        $places = Place::paginate(15);
        return view('admin.places.index', compact('places'));
    }


        public function savePlace(Request $request, Place $place)
    {

           
            $place->name = $request['name'];
            $place->address = $request['address'];
            $place->zip_code = $request['zip_code'];
            $place->city = $request['city'];
            $place->accroche = $request['accroche'];
            $place->extrait = $request['extrait'];
            $place->country = $request['country'];
            $place->phone = $request['phone'];
            $place->capacity = $request['capacity'];
            $place->surface = $request['surface'];
            $place->description = $this->upload_img($request->input('description'));    

            $place->save();

            $place->place_categories()->detach();

            $places_categories = Place_categorie::all();

            foreach ($places_categories as $places_category):

                $place->place_categories()->attach(Place_categorie::where('id', $request['place_category'.$places_category->id])->first());

            endforeach;

            $place->locations()->detach();

            $place->locations()->attach(Location::where('id', $request['location'])->first());

            return $place;
    }
 
}
