<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = [
        'name', 'zip_code', 'city', 'capacity', 'surface', 'accroche', 'description', 'extrait', 'country', 'address', 'description', 'phone',
    ];

    protected $dateFormat = 'Y-m-d H:i:s';

    public function place_categories()
    {
        return $this->belongsToMany('App\place_categorie', 'placescategories_places', 'place_id', 'placecategorie_id');
    }

    

    public function hasAnyPlace_categories($place_categories)
    {
        if (is_array($place_categories)) {
            foreach ($place_categories as $place_categorie) {
                if ($this->hasPlace_categorie($place_categorie)) {
                    return true;
                }
            }
        } else {
            if ($this->hasPlace_categorie($place_categorie)) {
                return true;
            }
        }
        return false;
    }
    
    public function hasPlace_categorie($place_categorie)
    {
        if ($this->place_categories()->where('name', $place_categorie)->first()) {
            return true;
        }
        return false;
    }

    public function locations()
    {
        return $this->belongsToMany('App\location', 'place_location', 'place_id', 'location_id');
    }

    public function hasAnyLocations($locations)
    {
        if (is_array($locations)) {
            foreach ($locations as $location) {
                if ($this->hasLocation($location)) {
                    return true;
                }
            }
        } else {
            if ($this->hasLocation($location)) {
                return true;
            }
        }
        return false;
    }
    
    public function hasLocation($location)
    {
        if ($this->locations()->where('name', $location)->first()) {
            return true;
        }
        return false;
    }

    public function room()
    {
        return $this->belongsToMany('App\Room', 'place_room', 'place_id', 'room_id');
    }
    

}
