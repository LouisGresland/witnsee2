<?php 

namespace App\Traits;

use Intervention\Image\ImageManagerStatic as Image;

trait Upload_img
{
     public function upload_img($description) {
     
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $dom->getElementsByTagName('img');

            foreach($images as $img){

                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){ 
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $filename = uniqid();
                    $filepath = 'images\lieux\\'.$filename.'.jpg';
                    $image = Image::make($src)->encode('jpg', 100)->save(
                    $filepath);                
                    $new_src = '/images/lieux/'.$filename.'.jpg';                    
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                    } 
                } 

                return $dom->saveHTML();

    }

    public function upload_imgsalles($description) {
     
            libxml_use_internal_errors(true);
            $dom = new \DOMDocument();
            $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);    
            $images = $dom->getElementsByTagName('img');

            foreach($images as $img){

                $src = $img->getAttribute('src');
                if(preg_match('/data:image/', $src)){ 
                    preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                    $filename = uniqid();
                    $filepath = 'images\salles\\'.$filename.'.jpg';
                    $image = Image::make($src)->encode('jpg', 100)->save(
                    $filepath);                
                    $new_src = '/images/salles/'.$filename.'.jpg';                    
                    $img->removeAttribute('src');
                    $img->setAttribute('src', $new_src);
                    } 
                } 

                return $dom->saveHTML();

    }
}
