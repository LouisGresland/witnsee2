<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'name', 'image',
    ];

    public $timestamps = false;

    public function placesloaction()
    {
        return $this->belongsToMany('App\Place', 'place_location', 'location_id', 'place_id');
    }
}
