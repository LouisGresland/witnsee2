<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place_categorie extends Model
{

	protected $fillable = [
        'name', 'description', 'image',
    ];
    public function places()
    {
        return $this->belongsToMany('App\Place', 'placecategories_places', 'placecategorie_id', 'place_id');
    }

    public $timestamps = false;

    
}