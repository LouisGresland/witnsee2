<!doctype html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <title>Portail d'administration</title>
	    <link rel="stylesheet" href="/css/admin.css">
	    <!-- include libraries(jQuery, bootstrap) -->
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.2/summernote.css" rel="stylesheet">




	</head>
	<body>

	@if(Auth::User()->hasRole('admin'))
		<a href="{{ url('/logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                             Se déconnecter
                </a>

       <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
       </form>
        <br/>

		<a href="{{ route('users.index') }}"> Utilisateurs </a>
		<br/>

		<a href="{{ route('places.index') }}"> Lieux </a>
		<br/>

		<a href="{{ route('place_categories.index') }}"> Catégorie de lieux </a>
		<br/>

		<a href="{{ route('locations.index') }}"> Localisations </a>
		<br/>

		@endif

		<a href="{{ url('/home') }}" > Retour à l'accueil </a>
		<br/>
		<div class="main">



		    @yield('content')

		</div>
		<script src="/js/all.js"></script>
	</body>
</html>