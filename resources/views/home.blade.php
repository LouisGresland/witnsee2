@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Tableau de bord</div>

                <div class="panel-body">
                    Vous êtes connecté

                    <br/>
                    

                    <a href="{{ route('users.edit', Auth::user()->id) }}" class="btn btn-default btn-sm">Mon compte</a>

                    @if(Auth::User()->hasRole('admin'))

                    {{ Html::link('admin/users', 'Portail administratif') }}

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
