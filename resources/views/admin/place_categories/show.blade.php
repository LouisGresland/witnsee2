@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif


	<div class="container">
				
			<p> Libellé : {{ $place_category->name }}</p>
			<p> Description : {{ $place_category->description }}</p>
			@if (isset($place_category->image) and $place_category->image != '')
        {!! Html::image('images/categories/' . $place_category->image, 'photo actuelle', ['class' => 'img-thumbnail']) !!}
        
    	@endif

			
							
							
			
				</div>
			</div>
				
		<div class="container center">
			<div class="well">
				
				<div class="row">
					<div class="col-sm-6">
						
						{!! Html::linkRoute('place_categories.edit', 'Modifier', array($place_category->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-sm-6">
						
						
						{!! Form::open(['route' => ['places.destroy', $place_category->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return ConfirmDeletePlacecategory()']) !!}

						{!! Form::close() !!}
						



						
					</div>
				</div>
					<br/>
				<div class="row">
					<div class="col-md-12">
						
						
						<a href="{{ route('place_categories.index', $place_category->id) }}" class="btn btn-default btn-block btn-h1-spacing" > << Voir toutes les catégories</a>
						

					</div>
				</div>

			</div>
		</div>
	</div>
<script>

  function ConfirmDelete()
  {
  var x = confirm("Etes vous sûr de vouvloir supprimer cet utilisateur?");
  if (x)
    return true;
  else
    return false;
  }

</script>

@endsection