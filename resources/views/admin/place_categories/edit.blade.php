@extends('layouts.admin')

@section('content')

<div class="row">

		{!! Form::model($place_category, ['route' => ['place_categories.update', $place_category->id], 'method' => 'PUT', 'files'=>true]) !!}

		

		

		{{ csrf_field() }}
		<div class="col-md-8">

			@include('admin.place_categories._form')

			
		</div>

			


		<div class="col-md-4">

			<div class="well">

				<div class="row">

					<div class="col-sm-6">
						

						<a href="{{ route('place_categories.show', $place_category->id) }}" class="btn btn-danger btn-block">Annuler</a>
						



					</div>
					<div class="col-sm-6">
						<input type="submit" value="Enregister" class="btn btn-success btn-block">
					</div>
				</div>

			</div>
				
		</div>
</form>




@stop