@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif



<div class="row">
        <div class="col-md-9">
            <h1>Toutes les categories</h1>
        </div>

       <div class="col-md-3">
            <a href="{{ route('place_categories.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Ajouter une catégorie</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>Libellé</th>
                     
                    <th></th>
                </thead>

                <tbody>
                    
                    @foreach ($place_categories as $place_category)
                        
                        <tr>
                            <th>{{ $place_category->name }}</th>
                            
                            <td><a href="{{ route('place_categories.show', $place_category->id) }}" class="btn btn-default btn-sm">Voir</a></td>
                            <td><a href="{{ route('place_categories.edit', $place_category->id) }}" class="btn btn-default btn-sm">Modifier</a></td>
                            <td>
                            {!! Form::open(['route' => ['place_categories.destroy', $place_category->id], 'method' => 'DELETE']) !!}

                            {!! Form::submit('Supprimer', ['class' => 'btn btn-default btn-sm', 'onclick' => 'return ConfirmDeletePlaceCategory()']) !!}

                            {{ Form::close() }}
                        </tr>

                    @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $place_categories->links() !!}
            </div>
        </div>
</div>





@endsection