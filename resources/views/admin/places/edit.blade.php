@extends('layouts.admin')

@section('content')

<div class="container">

		{!! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'PUT']) !!}

		

		

		{{ csrf_field() }}
		<div class="col-md-12">

			@include('admin.places._form')

			
		</div>

		<div class="col-md-2"></div>

		<div class="col-md-8">
				<a href="{{ route('rooms.index', $place->id) }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Les salles</a>
			</div>
		


		<div class="col-md-12">

			
			
				

			<div class="well">



				<div class="row">


					<div class="col-sm-6">
						

						<a href="{{ route('places.show', $place->id) }}" class="btn btn-danger btn-block">Annuler</a>
						



					</div>
					<div class="col-sm-6">
						<input type="submit" value="Enregister" class="btn btn-success btn-block">
					</div>
				</div>

			</div>
				
		</div>
</form>




@stop