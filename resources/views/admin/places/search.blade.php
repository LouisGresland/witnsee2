@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif

<div class="col-sm-3 col-md-3 pull-right">
            {!! Form::open(['url'=>'admin/places/search', 'class'=>'navbar-form']) !!}
            <div class="input-group">
                {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Libellé', 'id' => 'search']) !!}
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            {!! Form::close() !!}
</div>

<div class="row">
        <div class="col-md-10">
            <h1>Recherche de lieux</h1>
        </div>

       <div class="col-md-2">
            <a href="{{ route('places.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Ajouter un lieu</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>Libellé</th>
                    <th>Ville</th>
                    <th>Date de création</th>
                     
                    <th></th>
                </thead>

                <tbody>
                    
                    @foreach ($places as $place)
                        
                        <tr>
                            <th>{{ $place->name }}</th>
                            <td>{{ $place->city }}</td>
                            <td>{{ $place->created_at }}</td>
                            
                            <td><a href="{{ route('places.show', $place->id) }}" class="btn btn-default btn-sm">Voir</a></td>
                            <td><a href="{{ route('places.edit', $place->id) }}" class="btn btn-default btn-sm">Modifier</a></td>
                            <td>
                            {!! Form::open(['route' => ['places.destroy', $place->id], 'method' => 'DELETE']) !!}

                            {!! Form::submit('Supprimer', ['class' => 'btn btn-default btn-sm', 'onclick' => 'return ConfirmDeletePlace()']) !!}

                            {{ Form::close() }}
                        </tr>

                    @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $places->links(); !!}
            </div>
        </div>
</div>





@endsection