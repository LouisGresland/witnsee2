@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif


	<div class="container">
				
			<h1 class="text-center"> {{ $place->name }}</h1>

			<p> {!! $place->description !!} </p>
							
							
			<h3>Accroche :</h3> 
			<p>{{ $place->accroche }}</p>			
				
			<h3>Extrait : </h3> 
			<p>{{ $place->extrait }}</p>

			<p> Localisation : 
				@foreach ($locations as $location)       
               		{{ $place->hasLocation($location->name) ? $location->name  : '' }}
        		@endforeach
			<br/>
			Catégorie : <br/>
				@foreach ($place_categories as $place_category)       
               		 {!! $place->hasplace_categorie($place_category->name) ? '- '.$place_category->name.'<br/>' : '' !!} 
        		@endforeach
			<br/>
			</p>
				
			<p>Adresse : {{ $place->address }} <br/>
				Code postal : {{ $place->zip_code }} <br/>
				Ville : {{ $place->city }} <br/>
				Pays : {{ $place->country }} <br/>
				Téléphone : {{ $place->phone }}</p>
			<div class="text-right">
				<p>Capacité : {{ $place->capacity }} <br/>
				Superficie : {{ $place->surface }}</p>
				</div>
			</div>
				
		<div class="container center">
			<div class="well">
				
				<div class="row">
					<div class="col-sm-6">
						
						{!! Html::linkRoute('places.edit', 'Modifier', array($place->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-sm-6">
						
						
						{!! Form::open(['route' => ['places.destroy', $place->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return ConfirmDeletePlace()']) !!}

						{!! Form::close() !!}
						



						
					</div>
				</div>
					<br/>
				<div class="row">
					<div class="col-md-12">
						
						
						<a href="{{ route('places.index', $place->id) }}" class="btn btn-default btn-block btn-h1-spacing" > << Voir tous les lieux</a>
						

					</div>
				</div>

			</div>
		</div>
	</div>
<script>

  function ConfirmDelete()
  {
  var x = confirm("Etes vous sûr de vouvloir supprimer cet utilisateur?");
  if (x)
    return true;
  else
    return false;
  }

</script>

@endsection