<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

    {!! Form::label('name', 'Nom du lieu*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        {{ Form::text('name', old('name'), ["class" => 'form-control']) }}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

 <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">

    {!! Form::label('location', 'Localisation*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        @foreach ($locations as $location)
                        
                        
               <label id="location" class="radio-inline">{!! Form::radio('location', $location->id, Request::is('admin/places/create') ? "" : $place->hasLocation($location->name) ? 'checked' : '') !!} {{ $location->name }}</label>
                            

        @endforeach

        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
    </div>
</div>   
    
 <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">

    {!! Form::label('categories', 'Categories*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        @foreach ($places_categories as $places_category)
                        
                        
               <label id="category">{!! Form::checkbox('place_category'.$places_category->id, $places_category->id, Request::is('admin/places/create') ? "" : $place->hasPlace_categorie($places_category->name) ? 'checked' : '')   !!}  {{ $places_category->name }}</label><br/>
                            

        @endforeach

        @if ($errors->has('location'))
            <span class="help-block">
                <strong>{{ $errors->first('location') }}</strong>
            </span>
        @endif
    </div>
</div>   
    


<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    
    {!! Form::label('description', 'Description*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        

        {{ Form::textarea('description', old('description'), ["class" => 'form-control', "id" => 'description']) }}
            
    <script>
        $(document).ready(function() {
            $('#description').summernote();
        });
    </script>

    @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

</div>

<div class="form-group{{ $errors->has('accroche') ? ' has-error' : '' }}">
                            
    {!! Form::label('accroche', 'Accroche*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                            
        {{ Form::text('accroche', old('accroche'), ["class" => 'form-control']) }}

        @if ($errors->has('accroche'))
            <span class="help-block">
                <strong>{{ $errors->first('accroche') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('extrait') ? ' has-error' : '' }}">
                            
    {!! Form::label('extrait', 'Extrait*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                            
        {{ Form::text('extrait', old('extrait'), ["class" => 'form-control']) }}

        @if ($errors->has('extrait'))
            <span class="help-block">
                <strong>{{ $errors->first('extrait') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            
    {!! Form::label('address', 'Adresse*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                            
        {{ Form::text('address', old('address'), ["class" => 'form-control']) }}

        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">

    {!! Form::label('zip_code', 'Code postal*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
            
        {{ Form::text('zip_code', old('zip_code'), ["class" => 'form-control']) }}

        @if ($errors->has('zip_code'))
            <span class="help-block">
                <strong>{{ $errors->first('zip_code') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        
    {!! Form::label('city', 'Ville*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                            
        {{ Form::text('city', old('city'), ["class" => 'form-control']) }}

        @if ($errors->has('city'))
            <span class="help-block">
                <strong>{{ $errors->first('city') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
    
    {!! Form::label('country', 'Pays*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                                
        {{ Form::text('country', old('country'), ["class" => 'form-control']) }}

        @if ($errors->has('country'))
            <span class="help-block">
                <strong>{{ $errors->first('country') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('capacity') ? ' has-error' : '' }}">
    
    {!! Form::label('capacity', 'Capacité*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                                
        {{ Form::text('capacity', old('capacity'), ["class" => 'form-control']) }}

        @if ($errors->has('capacity'))
            <span class="help-block">
                <strong>{{ $errors->first('capacity') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('surface') ? ' has-error' : '' }}">
    
    {!! Form::label('surface', 'Superficie* (en m²)', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                                
        {{ Form::text('surface', old('surface'), ["class" => 'form-control']) }}

        @if ($errors->has('surface'))
            <span class="help-block">
                <strong>{{ $errors->first('surface') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
    
    {!! Form::label('phone', 'Téléphone', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
        
        {{ Form::text('phone', old('phone'), ["class" => 'form-control', 'placeholder' => '+33 x xx xx xx xx']) }}

        @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
                  