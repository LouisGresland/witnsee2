@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif


	<div class="container">
				
			<p> Libellé : {{ $location->name }}</p>
			@if (isset($location->image) and $location->image != '')
        {!! Html::image('images/locations/' . $location->image, 'photo actuelle', ['class' => 'img-thumbnail']) !!}
        
    	@endif
			
							
							
			
				</div>
			</div>
				
		<div class="container center">
			<div class="well">
				
				<div class="row">
					<div class="col-sm-6">
						
						{!! Html::linkRoute('locations.edit', 'Modifier', array($location->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-sm-6">
						
						
						{!! Form::open(['route' => ['locations.destroy', $location->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return ConfirmDeletePlacecategory()']) !!}

						{!! Form::close() !!}
						



						
					</div>
				</div>
					<br/>
				<div class="row">
					<div class="col-md-12">
						
						
						<a href="{{ route('locations.index', $location->id) }}" class="btn btn-default btn-block btn-h1-spacing" > << Voir toutes les catégories</a>
						

					</div>
				</div>

			</div>
		</div>
	</div>
<script>

  function ConfirmDelete()
  {
  var x = confirm("Etes vous sûr de vouvloir supprimer cet utilisateur?");
  if (x)
    return true;
  else
    return false;
  }

</script>

@endsection