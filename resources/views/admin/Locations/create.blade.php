@extends('layouts.admin')

@section('content')
<div class="row">

	@if(Session::has('error')) 
    <div class="alert alert-danger">
        {{ Session::get('error') }}
    </div>
@endif



		<div class="col-md-8 col-md-offset-2">

			<h1>Créer une nouvelle categorie</h1>
			<hr>

			{!! Form::open(array('route' => 'locations.store', 'data-parsley-validate' => '', 'files'=>true)) !!}

			{{ csrf_field() }}

			@include('admin.locations._form')

			<div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">

                                    Enregister

                                </button>
                            </div>
                        </div>
                    </form>

			</div>
</div>

@stop

