@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif



<div class="row">
        <div class="col-md-9">
            <h1>Toutes les localisations</h1>
        </div>

       <div class="col-md-3">
            <a href="{{ route('locations.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Ajouter une location</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>Libellé</th>
                     
                    <th></th>
                </thead>

                <tbody>
                    
                    @foreach ($locations as $location)
                        
                        <tr>
                            <th>{{ $location->name }}</th>
                            
                            <td><a href="{{ route('locations.show', $location->id) }}" class="btn btn-default btn-sm">Voir</a></td>
                            <td><a href="{{ route('locations.edit', $location->id) }}" class="btn btn-default btn-sm">Modifier</a></td>
                            <td>
                            {!! Form::open(['route' => ['locations.destroy', $location->id], 'method' => 'DELETE']) !!}

                            {!! Form::submit('Supprimer', ['class' => 'btn btn-default btn-sm', 'onclick' => 'return ConfirmDeleteLocation()']) !!}

                            {{ Form::close() }}
                        </tr>

                    @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $locations->links() !!}
            </div>
        </div>
</div>




@endsection