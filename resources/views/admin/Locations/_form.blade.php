<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

    {!! Form::label('name', 'Libellé*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        {{ Form::text('name', old('name'), ["class" => 'form-control']) }}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group">
    {!! Form::label('image', 'Image à la une (poids maximum : 1Mo)') !!}
    @if (isset($location->image) and $location->image != '')
        {!! Html::image('images/locations/' . $location->image, 'photo actuelle', ['class' => 'img-thumbnail']) !!}
        <p class="help-block">Pour modifier votre image, cliquez ci-dessous : </p>
    @endif
    {!! Form::file('image', array('class' => 'form-control')) !!}
    <small class="text-danger">{{ $errors->first('image') }}</small>
</div>

       
