<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

    {!! Form::label('name', 'Nom de la salle*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        {{ Form::text('name', old('name'), ["class" => 'form-control']) }}

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

    


<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
    
    {!! Form::label('description', 'Description*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        

        {{ Form::textarea('description', old('description'), ["class" => 'form-control', "id" => 'description']) }}
            
    <script>
        $(document).ready(function() {
            $('#description').summernote();
        });
    </script>

    @if ($errors->has('description'))
            <span class="help-block">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>

</div>


<div class="form-group{{ $errors->has('capacity') ? ' has-error' : '' }}">
    
    {!! Form::label('capacity', 'Capacité*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                                
        {{ Form::text('capacity', old('capacity'), ["class" => 'form-control']) }}

        @if ($errors->has('capacity'))
            <span class="help-block">
                <strong>{{ $errors->first('capacity') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('surface') ? ' has-error' : '' }}">
    
    {!! Form::label('surface', 'Superficie* (en m²)', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">
                                
        {{ Form::text('surface', old('surface'), ["class" => 'form-control']) }}

        @if ($errors->has('surface'))
            <span class="help-block">
                <strong>{{ $errors->first('surface') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('equipment') ? ' has-error' : '' }}">
    
    {!! Form::label('equipment', 'Equipement*', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        

        {{ Form::textarea('equipment', old('equipment'), ["class" => 'form-control', "id" => 'equipment']) }}
            
    <script>
        $(document).ready(function() {
            $('#equipment').summernote();
        });
    </script>

    @if ($errors->has('equipment'))
            <span class="help-block">
                <strong>{{ $errors->first('equipment') }}</strong>
            </span>
        @endif
    </div>

</div>   

<div class="form-group{{ $errors->has('constraints') ? ' has-error' : '' }}">
    
    {!! Form::label('constraints', 'Contraintes', array('class' => 'col-md-4 control-label')) !!}

    <div class="col-md-8">

        

        {{ Form::textarea('constraints', old('constraints'), ["class" => 'form-control', "id" => 'constraints']) }}
            
    <script>
        $(document).ready(function() {
            $('#constraints').summernote();
        });
    </script>

    @if ($errors->has('constraints'))
            <span class="help-block">
                <strong>{{ $errors->first('constraints') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('disabled_access') ? ' has-error' : '' }}">
    {!! Form::label('disabled_access', 'Accès handicapé*', array('class' => 'col-md-4 control-label')) !!}
    <div class="col-md-8">
        
                        
                        
               <label id="disabled_access" class="radio-inline">{!! Form::radio('disabled_access', '1') !!} Oui</label>

               <label id="disabled_access" class="radio-inline">{!! Form::radio('disabled_access', '0') !!} Non</label>

        @if ($errors->has('disabled_access'))
            <span class="help-block">
                <strong>{{ $errors->first('disabled_access') }}</strong>
            </span>
        @endif
    </div>
</div>

       
