@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif

<div class="col-sm-3 col-md-3 pull-right">
            {!! Form::open(['url'=>'admin/users/search', 'class'=>'navbar-form']) !!}
            <div class="input-group">
                {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Nom', 'id' => 'search']) !!}
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
            {!! Form::close() !!}
</div> 

<div class="row">
        <div class="col-md-10">
            <h1>Recherche d'utilisateurs</h1>
        </div>

        <div class="col-md-2">
            <a href="{{ route('users.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Ajouter un utilisateur</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
    </div> <!-- end of .row -->

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Email</th>
                    
                    
                    <th></th>
                </thead>

                <tbody>
                    
                    @foreach ($users as $user)
                        
                        <tr>
                            <th>{{ $user->name }}</th>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->email }}</td>
                            
                            <td><a href="{{ route('users.show', $user->id) }}" class="btn btn-default btn-sm">Voir</a></td>
                            <td><a href="{{ route('users.edit', $user->id) }}" class="btn btn-default btn-sm">Modifier</a></td>
                            <td>
                            {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE']) !!}

                            {!! Form::submit('Supprimer', ['class' => 'btn btn-default btn-sm', 'onclick' => 'return ConfirmDelete()']) !!}

                            {{ Form::close() }}


                            </td>
                        </tr>

                    @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $users->links(); !!}
            </div>
        </div>
</div>





@endsection