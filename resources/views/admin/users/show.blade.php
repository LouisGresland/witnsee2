@extends('layouts.admin')

@section('content')

@if(Session::has('success')) 
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif



<div class="row">
		<div class="col-md-8">


			<div id="backend-comments" style="margin-top: 50px;">

				<table class="table">
					<thead>
						<tr>
							<th>Nom</th>
							<th>Prénom</th>
							<th>Email</th>
							<th>Adresse</th>
							<th>Ville</th>
							<th>Code postal</th>
							<th>Pays</th>
							<th>Civilité</th>
							<th>Type d'utilisateur</th>
							<th>Nom de la société</th>
							<th>Poste</th>
							<th>Téléphone</th>
							<th width="70px"></th>
						</tr>
					</thead>
					<tr>
							<td>{{ $user->name }}</td>
							<td>{{ $user->first_name }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->address }}</td>
							<td>{{ $user->city }}</td>
							<td>{{ $user->postal_code }}</td>
							<td>{{ $user->country }}</td>
							<td>{{ $user->civility }}</td>
							<td>@if($user->society == '0')
									{{ $type = 'Particulier' }}
										@else
										{{ $type = 'Société' }}
										@endif</td>
							<td>{{ $user->society_name }}</td>
							<td>{{ $user->poste }}</td>
							<td>{{ $user->phone }}</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-4">
			<div class="well">
				
				<div class="row">
					<div class="col-sm-6">
						
						{!! Html::linkRoute('users.edit', 'Modifier', array($user->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>
					<div class="col-sm-6">
						
						@if(Auth::User()->hasRole('admin'))
						{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE']) !!}

						{!! Form::submit('Supprimer', ['class' => 'btn btn-danger btn-block', 'onclick' => 'return ConfirmDelete()']) !!}

						{!! Form::close() !!}
						@endif



						
					</div>
				</div>
					<br/>
				<div class="row">
					<div class="col-md-12">
						
						@if(Auth::User()->hasRole('admin'))
						<a href="{{ route('users.index', $user->id) }}" class="btn btn-default btn-block btn-h1-spacing" > << Voir tous les utilisateurs</a>
						@endif

						


					</div>
				</div>

			</div>
		</div>
	</div>
<script>

  function ConfirmDelete()
  {
  var x = confirm("Etes vous sûr de vouvloir supprimer cet utilisateur?");
  if (x)
    return true;
  else
    return false;
  }

</script>

@endsection