
<div class="form-group{{ $errors->has('civility') ? ' has-error' : '' }}">


    {!! Form::label('civility', 'Civilité*', array('class' => 'col-md-4 control-label'), array('for' => 'civility')) !!}


    <div class="col-md-6">

    <label id="civility" class="radio-inline">{!! Form::radio('civility', 'Mr') !!}Mr</label>

    <label id="civility" class="radio-inline">{!! Form::radio('civility', 'Mme') !!}Mme</label>


    @if ($errors->has('civility'))
    <span class="help-block">
        <strong>{{ $errors->first('civility') }}</strong>
    </span>
    @endif
    </div>
    </div>

    

                        
    <div class="form-group{{ $errors->has('society') ? ' has-error' : '' }}">

    {!! Form::label('society', 'Je suis*', array('class' => 'col-md-4 control-label'), array('for' => 'society')) !!}

    <div class="col-md-6">

    <label id="" class="radio-inline" OnChange="showFieldType()">{!! Form::radio('society', '1') !!}Une société</label>

    <label id="" class="radio-inline" OnChange="showFieldType()">{!! Form::radio('society', '0' ) !!}Un particulier</label>


    @if ($errors->has('society'))
        <span class="help-block">
            <strong>{{ $errors->first('society') }}</strong>
        </span>
    @endif
    </div>
    </div>

                        

    <div id="society_name" class="form-group{{ $errors->has('society_name') ? ' has-error' : '' }} {{ $society == "1" ? 'display_block' : 'display_none' }}">

        {!! Form::label('society_name', 'Société*', array('class' => 'col-md-4 control-label'), array('for' => 'society_name')) !!}

            <div class="col-md-6">
                                

        {{ Form::text('society_name', old('society_name'), array('id' => 'society_name_input', 'class' => 'form-control')) }}

        @if ($errors->has('society_name'))
            <span class="help-block">
                <strong>{{ $errors->first('society_name') }}</strong>
            </span>
        @endif
        </div>
        </div>


        <div id="poste" class="form-group{{ $errors->has('poste') ? ' has-error' : '' }} {{ $society == "1" ? 'display_block' : 'display_none'}}">
                           
            {!! Form::label('poste', 'Poste*', array('class' => 'col-md-4 control-label')) !!}



            <div class="col-md-6">
                               

            {{ Form::text('poste', old('poste'),array('id' => 'poste_input', 'class' => 'form-control')) }}

            @if ($errors->has('poste'))
                <span class="help-block">
                    <strong>{{ $errors->first('poste') }}</strong>
                </span>
            @endif
            </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

            {!! Form::label('name', 'Nom*', array('class' => 'col-md-4 control-label')) !!}

            <div class="col-md-6">

            {{ Form::text('name', old('name'), ["class" => 'form-control']) }}

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            {!! Form::label('first_name', 'Prénom*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('first_name', old('first_name'), ["class" => 'form-control']) }}

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            {!! Form::label('address', 'Adresse*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('address', old('address'), ["class" => 'form-control']) }}

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            {!! Form::label('city', 'Ville*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('city', old('city'), ["class" => 'form-control']) }}

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('postal_code') ? ' has-error' : '' }}">
                            {!! Form::label('postal_code', 'Code postal*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('postal_code', old('postal_code'), ["class" => 'form-control']) }}

                                @if ($errors->has('postal_code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postal_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            {!! Form::label('country', 'Pays*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('country', old('country'), ["class" => 'form-control']) }}

                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            {!! Form::label('phone', 'Téléphone*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('phone', old('phone'), ["class" => 'form-control']) }}

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            {!! Form::label('email', 'Adresse email*', array('class' => 'col-md-4 control-label')) !!}

                            <div class="col-md-6">
                                {{ Form::text('email', old('email'), ["class" => 'form-control']) }}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if(Auth::check())
                        @if(Auth::User()->hasRole('admin'))

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} {{ Request::is('admin/users/create') ? "display_none" : "" }}">
                        {!! Form::label('role', 'Role :', array('class' => 'col-md-4 control-label', 'id' => 'editPassword')) !!}

                        <div class="col-md-6 control-label {{ Request::is('admin/users/create') ? "display_none" : "" }}" id="editPassword">
                        <input type="checkbox" {{ $user->hasRole('utilisateur') ? 'checked' : '' }} name="role_user">{{ Form::label('user', 'Utilisateur') }}
                        </div>

                        <div class="col-md-4"></div>

                        <div class="col-md-6 control-label {{ Request::is('admin/users/create') ? "display_none" : "" }}" id="editPassword">
                        <input type="checkbox" {{ $user->hasRole('prestataire') ? 'checked' : '' }} name="role_prestataire">
                        {{ Form::label('prestataire', 'Prestataire') }}
                        </div>

                        <div class="col-md-4"></div>

                        <div class="col-md-6 control-label {{ Request::is('admin/users/create') ? "display_none" : "" }}" id="editPassword">
                        <input type="checkbox" {{ $user->hasRole('gérant de lieu') ? 'checked' : '' }} name="role_gerant">
                        {{ Form::label('gerant', 'Gérant de lieu') }}



                        </div>

                        <div class="col-md-4"></div>

                        <div class="col-md-6 control-label {{ Request::is('admin/users/create') ? "display_none" : "" }}" id="editPassword">
                        <input type="checkbox" {{ $user->hasRole('admin') ? 'checked' : '' }} name="role_admin">
                        {{ Form::label('admin', 'Administrateur') }}
                        </div>
                </div>

                
                @endif
                @endif
                
            
            
            <div class="col-md-10 control-label {{ Request::is('admin/users/create') ? "display_none" : "" }}" id="editPassword">
            <label id="ShowPassword" name="ShowPassword" class="" OnChange="showPassword()" style="">{!! Form::checkbox('showpassword', '1') !!}Modifier mon mot de passe</label>
            </div>
            
           

            

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} {{ Request::is('users/'.$id.'/edit') ? "display_none" : "" }}" id="password2" style="">
                            <label for="password" class="col-md-4 control-label">Mot de passe*</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" value="">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} {{ Request::is('users/'.$id.'/edit') ? "display_none" : "" }}" id="password3">

                            <label for="password-confirm" class="col-md-4 control-label">Confirmation du mot de passe*</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                  