@extends('layouts.admin')

@section('content')

<div class="row">

		{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}

		

		

		{{ csrf_field() }}
		<div class="col-md-8">

			@include('admin.users._form')

			
		</div>

			


		<div class="col-md-4">

			<div class="well">

				<div class="row">

					<div class="col-sm-6">
						

						<a href="{{ route('users.show', $user->id) }}" class="btn btn-danger btn-block">Annuler</a>
						



					</div>
					<div class="col-sm-6">
						<input type="submit" value="Enregister" class="btn btn-success btn-block">
					</div>
				</div>

			</div>
				
		</div>
</form>




@stop