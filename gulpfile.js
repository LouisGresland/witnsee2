const elixir = require('laravel-elixir');

require('laravel-elixir-vue');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


elixir(function(mix) {
	mix.sass ([
		'app.scss',
		'public.scss'
		], 'public/css/public.css');
});

elixir(function(mix) {
	mix.sass ([
		'summernote.scss',
		'app.scss',
		'admin.scss'		
		], 'public/css/admin.css');
});

elixir(function(mix) {
    mix.scripts([
    	'summernote.js',
        'app.js',
        'bootstrap.js',
        'confirmdelete.js',
        'ShowFieldType.js',
        'showPassword.js',   
        'summernote-fr-FR.js'
    ]);
});

elixir(function(mix){
	mix.copy(
		'./resources/assets/fonts', 'public/fonts'	
	);		
});







